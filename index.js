/*
    ATLAS BOT LIST
    2021-2022

    The code you see here is an archive of the code that was used to host the list.
    This code was originally licensed under the GNU GPLv3 license, even while the list was up.
    The only modified content between this and original code is this big comment block and some comments in the settings file. (and the README, if you count that)
    The code was originally developed by the following people:
    - Rizon
    - DLuxe
    - Tazhys
    - Toxic Dev

    Feel free to use the code, but please give credit where its due.
*/
require("module-alias/register"); // for modules aliases

const { Client, Collection } = require("discord.js");
const config = require("@settings");
const client = new Client({ allowedMentions: { repliedUser: false, parse: ["users"] }, disabledEvents: ["TYPING_START"], intents: 32767 });
const events = require("./structures/event");
const command = require("./structures/command");

client.commands = new Collection();
client.aliases = new Collection();
client.limits = new Map();
client.config = config;

client.on("ready", () => {
    console.log("Bot is ready!");
});

command.run(client);
events.run(client);
client.login(config.token)
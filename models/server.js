const { Schema, model } = require("mongoose");

module.exports = model("discordservers", new Schema({
    //String
    id: { type: String, default: "" },
    servername: { type: String, default: "" },
    owner: { type: String, default: "" },
    description: { type: String, default: "" },
    short: { type: String, default: "" },
    website: { type: String, default: "" },
    invite: { type: String, default: "" },
    announce: { type: String, default: "" },

    // Boolean
    affirmed: { type: Boolean, default: true },
    nsfw: { type: Boolean, default: false },

    //Arrays
    tags: { type: Array, default: []},
}));
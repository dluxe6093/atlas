const { Schema, model } = require("mongoose");

module.exports = model("discordbots", new Schema({
    //String
    id: { type: String, default: "" },
    username: { type: String, default: "" },
    owner: { type: String, default: "" },
    description: { type: String, default: "" },
    short: { type: String, default: "" },
    website: { type: String, default: "" },
    donate: { type: String, default: "" },
    invite: { type: String, default: "" },
    server: { type: String, default: "" },
    prefix: { type: String, default: "" },
    auth: { type: String, default: "" },
    webauth: { type: String, default: "" },
    rreason: { type: String, default: "" },
    reportedby: { type: String, default: "" },
    vanity: { type: String, default: "" },
    announce: { type: String, default: "" },
    banner: { type: String, default: "" },

    // Int
    servers: { type: Number, default: 0 },
    shards: { type: Number, default: 0 },
    votes: { type: Number, default: 0 },
    date: { type: Number, default: Date.now() },

    // Boolean
    affirmed: { type: Boolean, default: false },
    nsfw: { type: Boolean, default: false },
    verified: { type: Boolean, default: false },
    reported: { type: Boolean, default: false },
    certified: { type: Boolean, default: false },

    //Arrays
    tags: { type: Array, default: []},
    lib: { type: Array, default: []},
    owners: { type: Array, default: []}
}));
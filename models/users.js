const { Schema, model } = require("mongoose");

module.exports = model("discordusers", new Schema({
    //String
    id: { type: String, default: "" },
    bio: { type: String, default: "" },
    donate: { type: String, default: "" },
    github: { type: String, default: "" },
    website: { type: String, default: "" },
    twitter: { type: String, default: "" },
    botliked: { type: Array, default: ""},
    time: { type: Date, default: () => new Date(Date.now()) },

    //Maps/Array's
    votes: { type: Map, of: String, default: {} },
    badges: { type: Map, default: {
        booster: false,
        donator: false,
        premium: false,
        beta: false,
        certified: false,
        moderator: false,
        developer: false,
        admin: false,
        owner: false
    }}
}));
const sitemaps = require('sitemaps');
const config = require("@settings");

class sitemap {
    constructor(){}

    async start(){
        const links = [
            {
                loc: `${config.domain}/`,
                priority: '1.00',
                changefreq: 'monthly',
            },
            {
                loc: `${config.domain}/tos`,
                priority: '1.00',
                changefreq: 'monthly',
            },
            {
                loc: `${config.domain}/discord`,
                priority: '1.00',
                changefreq: 'monthly',
            }
        ];
        sitemaps("./website/public/assets/others/sitemap.xml", links);
    }
}

module.exports = sitemap;
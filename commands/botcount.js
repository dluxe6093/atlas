const { MessageEmbed } = require("discord.js");
const bots = require("@bots");
module.exports.run = async (client, message, args) => {
    let bot = await bots.find();
    return await message.reply({ content: `There are currently **${bot.length}** bots listed on the site.` })
}

module.exports.help = {
    name: "botcount",
    category: "main",
    aliases: ['bc', 'count'],
    description: "Get how many bots are listed on the site.",
    example: "``botcount``"
}

module.exports.requirements = {
    userPerms: [],
    clientPerms: [],
    ownerOnly: false,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

const { MessageEmbed } = require("discord.js");
const bots = require("@bots");
module.exports.run = async (client, message, args) => {
    if (!message.member.roles.cache.get("940046849958629447")) return message.reply("You don't have permission to use this command.");
    if (!args[0]) return await message.reply("Please specify a bot to delete");
    if (!args[1]) return await message.reply("Please specify a reason for deleting this bot");
    let userbot = message.mentions.users.first() || client.users.cache.get(args[0]) || await client.users.fetch(args[0]).catch(() => { });
    if (!userbot) return await message.reply("Bot does not exist");
    if (!userbot.bot) return await message.reply("This user is not a bot");
    let bot = await bots.findOne({ id: args[0] });
    if (!bot) return await message.reply("Bot does not exist");
    if (!bot.affirmed) return await message.reply("Bot has not been approved, you can deny it on the dashboard");
    await bot.deleteOne();
    await message.reply("Successfully removed bot");
    let chan = client.channels.cache.get("957429662550732850");
    let embed = new MessageEmbed()
        .setAuthor('Bot Deleted')
        .setColor('RED')
        .addField('Bot', `<@${userbot.id}> (\`${userbot.id}\`)`)
        .addField("Moderator", `${message.author.username} (\`${message.author.id}\`)`)
        .addField('Reason', args.slice(1).join(" "))
        .setThumbnail(userbot.displayAvatarURL({ format: "png", size: 256 }))
        .setTimestamp();
    await chan.send({ embeds: [embed], content: `<@${bot.owner}>` });
    let inserver = client.guilds.cache.get("940042260010106910").members.fetch(userbot.id).catch(() => { });
    if (inserver) await inserver.kick();
}

module.exports.help = {
    name: "remove",
    category: "staff",
    aliases: ['rem'],
    description: "Remove a bot.",
    example: "``remove <bot> <reason>``"
}

module.exports.requirements = {
    userPerms: ["MANAGE_GUILD"],
    clientPerms: [],
    ownerOnly: false,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

const { MessageEmbed, MessageCollector } = require("discord.js");
const bots = require("@bots");
module.exports.run = async(client, message, args) => {
    if (!args[0]) return await message.reply({ content: "Specify a bot to certify." })
    let mention = message.mentions.users.first() || client.users.cache.get(args[0]) || await client.users.fetch(args[0]).catch(() => {});
    if (!mention) return await message.reply({ content: "Specify a bot to certify." })
    if (!mention.bot) return await message.reply({ content: "Specify a bot to certify." })
    let bot = await bots.findOne({ id: mention.id });
    if (!bot) return await message.reply({ content: "Bot not found." })


    let emb = new MessageEmbed()
        .setColor(0xFF5733)
        .setTitle("Certify Bot")
        .setDescription(`You're about to certify <@${mention.id}>. Are you sure you want to do this?`)
    let m = await message.reply({ embeds: [emb] });
    let collect = new MessageCollector(message.channel, { filter: (m) => m.author.id == message.author.id, max: 1, time: 30000 });
    let chan = client.channels.cache.get("943004171886538762")
    collect.on("collect", async msg => {
        await m.delete().catch(() => {});
        if (msg.content != "y") return await msg.reply({ content: "Cancelled." });
        await bots.updateOne({ id: mention.id }, { $set: { certified: true } });

        await chan.send({ content: `<@${mention.id}> was **certified** by a Community Manager. Congratulations!` });

        return await msg.reply({ content: `Successfully certified <@${mention.id}>` });
    })
    collect.on("end", async(collected) => {
        if (collected.size == 0) {
            await m.delete().catch(() => {});
            return await message.reply({ content: "Request timed out." });
        }
    })
}

module.exports.help = {
    name: "certify",
    category: "main",
    aliases: ['cert'],
    description: "certify",
    example: "``certify <id>``"
}

module.exports.requirements = {
    userPerms: [],
    clientPerms: ["EMBED_LINKS"],
    comOnly: true,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

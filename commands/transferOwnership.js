const { MessageEmbed, MessageCollector } = require("discord.js");
const bots = require("@bots");
module.exports.run = async (client, message, args) => {
    if (!args[0]) return await message.reply({ content: "Specify a bot to transfer." })
    let mention = message.mentions.users.first() || client.users.cache.get(args[0]) || await client.users.fetch(args[0]).catch(() => {});
    if (!mention) return await message.reply({ content: "Specify a bot to transfer." })
    if (!mention.bot) return await message.reply({ content: "Specify a bot to transfer." })
    let bot = await bots.findOne({ id: mention.id });
    if (!bot) return await message.reply({ content: "Bot not found." })
    if (bot.owner != message.author.id) return await message.reply({ content: "You are not the owner of this bot." });
    let newOwner = message.mentions.users.first() || client.users.cache.get(args[1]) || await client.users.fetch(args[1]).catch(() => {});
    if (!newOwner) return await message.reply({ content: "Specify a new owner." })
    if (newOwner.bot) return await message.reply({ content: "Specify a new owner." });
    if (newOwner.id == bot.owner) return await message.reply({ content: "Specify a new owner." });
    let emb = new MessageEmbed()
    .setColor(0xFF5733)
    .setTitle("Transfer Ownership")
    .setDescription(`You are about to transfer the ownership of **${mention.username}** to **${newOwner.username}**. They will have full access to everything on the bot page and this is irreversible unless they transfer it back to you.\n\nAre you sure you want to do this? Type \`y\` to confirm or anything else to cancel.`)
    let m = await message.reply({ embeds: [emb] });
    let collect = new MessageCollector(message.channel, { filter: (m) => m.author.id == message.author.id, max: 1, time: 30000 });
    let chan = client.channels.cache.get("957429662550732850")
    collect.on("collect", async msg => {
        await m.delete().catch(() => {});
        if (msg.content != "y") return await msg.reply({ content: "Cancelled." });
        await bots.updateOne({ id: mention.id }, { $set: { owner: newOwner.id } });
        await chan.send({ content: `<@${message.author.id}> transferred the ownership of <@${mention.id}> to <@${newOwner.id}>.` });
        return await msg.reply({ content: "Successfully transferred ownership." });
    })
    collect.on("end", async (collected) => {
        if (collected.size == 0) {
            await m.delete().catch(() => {});
            return await message.reply({ content: "Request timed out." });
        }
    })
}

module.exports.help = {
    name: "transfer",
    category: "main",
    aliases: ['town'],
    description: "Transfer ownership of a bot.",
    example: "``transfer <bot> <user>``"
}

module.exports.requirements = {
    userPerms: [],
    clientPerms: [],
    ownerOnly: false,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

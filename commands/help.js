const { MessageEmbed } = require("discord.js");
module.exports.run = async (client, message, args) => {
    let emb = new MessageEmbed()
    .setDescription(`**Atlas Bot List**`).setColor(message.guild.me.displayColor)
    .addField("Website", `[Click here](https://atlasbots.com)`, true)
    .addField("Twitter", `[Click here](https://twitter.com/atlasbotlist)`, true)
    .addField("Bot Commands", `**Main**: help, botinfo, botcount, town\n**Staff**: eval, remove`);
    return await message.reply({ embeds: [emb] });
}

module.exports.help = {
    name: "help",
    category: "main",
    aliases: ['bi', 'b'],
    description: "Get the command list.",
    example: "``help``"
}

module.exports.requirements = {
    userPerms: [],
    clientPerms: [],
    ownerOnly: false,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

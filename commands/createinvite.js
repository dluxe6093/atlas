const { MessageEmbed } = require("discord.js");
module.exports.run = async (client, message, args) => {

    const botID = args.join(" ");
    
    const embedo = new MessageEmbed()
    .setDescription(`<:454716382869716992:740548147369213953> You haven't provided a bot ID`)
    .setColor("RED")
    
    if (!args[0]) return message.reply({embeds: [embedo]});
    
    let emb = new MessageEmbed()
      .setDescription(`<:454716382886494208:740548147318751263> here is the [invite](https://discord.com/oauth2/authorize?client_id=${botID}&scope=bot&permissions=0&guild_id=940042260010106910)`)
      .setColor("GREEN")
    return await message.reply({ embeds: [emb] });
}

module.exports.help = {
    name: "createinvite",
    category: "main",
    aliases: ['ci', 'c'],
    description: "create invite.",
    example: "``ci <botid>``"
}

module.exports.requirements = {
    userPerms: [],
    clientPerms: [],
    ownerOnly: false,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

const { MessageEmbed } = require("discord.js");
const bots = require("@bots");
module.exports.run = async(client, message, args) => {
    if (!args[0]) return await message.reply({ content: "Specify a bot to get information on." })
    let mention = message.mentions.users.first() || client.users.cache.get(args[0]) || await client.users.fetch(args[0]).catch(() => {});
    if (!mention) return await message.reply({ content: "Specify a bot to get information on." })
    if (!mention.bot) return await message.reply({ content: "Specify a bot to get information on." })
    let bot = await bots.findOne({ id: mention.id });
    if (!bot) return await message.reply({ content: "Bot not found." })
    let emb = new MessageEmbed()
        .setTitle(`${mention.username}`).setColor(message.guild.me.displayColor).addField("ID", `${mention.id}`, true).addField("Owner", `<@${bot.owner}>`, true).addField("Servers", `${bot.servers || 0}`, true).addField(`Votes`, `${bot.votes}`, true).addField("Approved", `${bot.affirmed}`, true).addField("Certified", `${bot.certified}`, true).addField("Added at", `${new Date(bot.date).toLocaleString()}`, true).addField(`Tags`, `${bot.tags[0] ? bot.tags.join(", ") : "None"}`).setThumbnail(mention.displayAvatarURL({ format: "png", size: 256 }));
    return await message.reply({ embeds: [emb] })
}

module.exports.help = {
    name: "botinfo",
    category: "main",
    aliases: ['bi', 'b'],
    description: "Get information about a bot.",
    example: "``botinfo <bot>``"
}

module.exports.requirements = {
    userPerms: [],
    clientPerms: [],
    ownerOnly: false,
    dm_only: false
}

module.exports.limits = {
    rateLimit: 2,
    cooldown: 1e4
}

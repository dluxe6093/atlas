const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
     message.delete();
                let normalContent = args.join(" ")
                message.channel.send({content: `${normalContent}`}, { disableEveryone: true })
}

module.exports.help = {
  name: "sudo",
  category: "staff",
  aliases: ['s'],
  description: "make the bot say something",
  example: "``say query``"
}

module.exports.requirements = {
  userPerms: [],
  clientPerms: ["EMBED_LINKS"],
  ownerOnly: true,
  dm_only: false
}

module.exports.limits = {
  rateLimit: 2,
  cooldown: 1e4
}

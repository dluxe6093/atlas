const submit = () => {
    let limit = false;
    let data = {
        id: $("#id").val(),
        prefix: $("#prefix").val(),
        short: $("#short").val(),
        invite: $("#invite").val(),
        server: $("#server").val(),
        website: $("#website").val(),
        donate: $("#donate").val(),
        auth: $("#auth").val(),
        tags: $("#tags").val(),
        long: $("#long").val(),
        users: $("#users").val().split(", ").filter(el => el),
        nsfw: $("#nsfw").prop('checked'),
        libraries: $("#libraries").val()
    }
    if(limit === false) {
        limit = true;
        $.ajax({
            type: "POST",
            data: JSON.stringify({data}),
            url: `/bot/add`,
            dataType: "json",
            contentType: 'application/json',
            success: (res) => {
                limit = false
                if(res.error) tata.error("Error", res.message)
                else {
                    tata.success("Success", "Your bot is pending verification!");
                    location.replace("/")
                }
            }
        });
    }
}
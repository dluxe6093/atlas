const report = (id) => {
    let data = {
        id: id,
        reportreason: $("#reportreason").val()
    }
    $.ajax({
        type: "POST",
        data: JSON.stringify({ data }),
        url: `/bot/report`,
        dataType: "json",
        contentType: 'application/json',
        success: (res) => {
            if (res.error) tata.error("Error", res.message)
            else {
                tata.success("Success", "You have successfully reported this bot!")
                location.replace("/")
            }
        }
    });
}
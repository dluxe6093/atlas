async function approve(id) {
    let data = {
        id: id
    }
    await Swal.fire({
        title: "Approving Bot",
        html: "Are you sure you want to approve this bot?",
        showCancelButton: true,
        confirmButtonText: "Approve",
        showLoaderOnConfirm: true,
        preConfirm: () => {
            $.ajax({
                type: "POST",
                data: JSON.stringify({ data }),
                url: `/bot/approve`,
                dataType: "json",
                contentType: 'application/json',
                success: (res) => {
                    if(res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    location.reload();
                }
                }
            })
        }
    })
}

async function decline(id) {
    let data = {
        id: id,
    }
    await Swal.fire({
        title: `Denying Bot`,
        html: `Enter a reason to deny this bot.`,
        showCancelButton: true,
        input: "text",
        confirmButtonText: `Deny`,
        showLoaderOnConfirm: true,
        preConfirm: (reason) => {
            data.reason = reason;
            $.ajax({
                type: "POST",
                data: JSON.stringify({ data }),
                url: `/bot/decline`,
                dataType: "json",
                contentType: 'application/json',
                success: (res) => {
                    if(res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    location.reload();
                }
                }
            })
        }
    })
}

async function rdelete(id) {
    let data = {
        id: id,
    }
    await Swal.fire({
        title: `Removing Bot`,
        html: `Enter a reason to remove this bot.`,
        showCancelButton: true,
        input: "text",
        confirmButtonText: `Remove`,
        showLoaderOnConfirm: true,
        preConfirm: (reason) => {
            data.rreason = reason;
            $.ajax({
                type: "POST",
                data: JSON.stringify({ data }),
                url: `/bot/report/delete`,
                dataType: "json",
                contentType: 'application/json',
                success: (res) => {
                    if(res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    location.reload();
                }
                }
            })
        }
    })
}

async function remove(id) {
    let data = {
        id: id
    }
    await Swal.fire({
        title: `Removing Report`,
        html: `Are you sure you want to remove this report?`,
        showCancelButton: true,
        confirmButtonText: `Remove`,
        showLoaderOnConfirm: true,
        preConfirm: () => {
            $.ajax({
                type: "POST",
                data: JSON.stringify({ data }),
                url: `/bot/report/remove`,
                dataType: "json",
                contentType: 'application/json',
                success: (res) => {
                    if(res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    location.reload();
                }
                }
            })
        }
    })
}

async function certify(id, owner) {
    let data = {
        id: id,
        owner: owner
    }
    await Swal.fire({
        title: "Certified Bot",
        html: "Are you sure you want to certify this bot?",
        showCancelButton: true,
        confirmButtonText: "Certify",
        showLoaderOnConfirm: true,
        preConfirm: () => {
            $.ajax({
                type: "POST",
                data: JSON.stringify({ data }),
                url: `/bot/certify`,
                dataType: "json",
                contentType: 'application/json',
                success: (res) => {
                    if(res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    console.log("Certified.")
                    location.reload();
                }
                }
            })
        }
    })
}

async function rcert(id, owner) {
    let data = {
        id: id,
        owner: owner
    }
    await Swal.fire({
        title: `Removing Bot`,
        html: `Enter a reason to uncert this bot.`,
        showCancelButton: true,
        input: "text",
        confirmButtonText: `Remove`,
        showLoaderOnConfirm: true,
        preConfirm: (reason) => {
            data.reason = reason;
            $.ajax({
                type: "POST",
                data: JSON.stringify({ data }),
                url: `/bot/certify/remove`,
                dataType: "json",
                contentType: 'application/json',
                success: (res) => {
                    if(res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    console.log("Removed Certification.")
                    location.reload();
                }
                }
            })
        }
    })
}
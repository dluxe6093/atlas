$("#search").focus(() => {
    $("#gosearch").addClass("visible")
});

$("#search").focusout(() => {
    setTimeout($("#gosearch").removeClass("visible"), 1000)
});

const go = () => {
    if ($("#search").val() == "") $("#search").addClass("fill").delay(2200).queue((next) => { $("#search").removeClass("fill"); next(); });
    else location.href = location.href = `search?q=${$("#search").val()}`;
};

$(document).ready(function(){
    $('#search').keypress(function(e){
      if(e.keyCode==13)
      $('#gosearch').click();
    });
});

const popup = (page) => {

    document.getElementsByClassName("close")[0].onclick = () => {
        $("#modal").fadeOut("300");
    };

    window.onclick = (event) => {
        if (event.target ==  document.getElementById("modal")) $("#modal").fadeOut("300");
    };

    document.getElementById("pagetext").innerHTML = document.getElementById(page).innerHTML;
    $("#modal").fadeIn("300");
};
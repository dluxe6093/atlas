const info = (id) => {
    $("#infobox").addClass("display");
    document.getElementsByName("infos").forEach(el => {
        el.setAttribute("class", "pInfoCardData invisible")
    })
    $(`#${id}`).removeClass("invisible");
};

const save = () => {
    let limit = false;
    let data = {
        bio: $("#bio").val(),
        donate: $("#donate").val(),
        github: $("#github").val(),
        website: $("#website").val(),
        twitter: $("#twitter").val()
    }
    if (limit === false) {
        limit = true;
        $.ajax({
            type: "POST",
            data: JSON.stringify({ data }),
            url: `/profile/settings`,
            dataType: "json",
            contentType: 'application/json',
            success: (res) => {
                if (res.error) tata.error("Error", res.message);
                else tata.success("Success", "Your changes has been changed");
                limit = false
            }
        });
    }
}

const deletebot = (id) => {
    let limit = false;
    let data = {
        id: id
    }
    if (limit === false) {
        limit = true;
        /* await Swal.fire({
            title: "Deleting Bot",
            html: "Are you sure you want to delete this bot?",
            showCancelButton: true,
            confirmButtonText: "Delete",
            showLoaderOnConfirm: true,
            preConfirm: () => { */
        $.ajax({
            type: "POST",
            data: JSON.stringify({ data }),
            url: `/bot/delete`,
            dataType: "json",
            contentType: 'application/json',
            success: (res) => {
                if (res.error) tata.error("Error", res.message);
                else {
                    tata.success("Success", res.message);
                    $(`#${id}`).fadeOut(300);
                    $(`#${id}av`).fadeOut(300);
                    $(`#infobox`).fadeOut(300);
                }
                limit = false;
                location.reload();
            }
        });
        // }
        //})
    }
}

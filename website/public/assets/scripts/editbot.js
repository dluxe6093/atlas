const save = (id) => {
    let limit = false;
    let data = {
        id: id,
        prefix: $("#prefix").val(),
        short: $("#short").val(),
        invite: $("#invite").val(),
        server: $("#server").val(),
        website: $("#website").val(),
        donate: $("#donate").val(),
        auth: $("#auth").val(),
        tags: $("#tags").val(),
        long: $("#long").val(),
        users: $("#users").val().split(", ").filter(el => el),
        nsfw: $("#nsfw").prop('checked'),
        vanity: $("#vanity").val(),
        announce: $("#announce").val(),
        libraries: $("#libraries").val(),
        banner: $("#banner").val()
    }
    if (limit === false) {
        limit = true;
        $.ajax({
            type: "POST",
            data: JSON.stringify({ data }),
            url: `/bot/${id}/save`,
            dataType: "json",
            contentType: 'application/json',
            success: (res) => {
                if (res.error) tata.error("Error", res.message);
                else tata.success("Success", "Your bot changes has been saved!");
                limit = false
                location.reload()
            }
        });
    }
}
const deletebot = (id) => {
    let limit = false;
    let data = {
        id: id
    }
    if (limit === false) {
        limit = true;
        Swal.fire({
            title: "Deleting Bot",
            html: "Are you sure you want to delete this bot?",
            showCancelButton: true,
            confirmButtonText: "Delete",
            showLoaderOnConfirm: true,
            preConfirm: () => {
                $.ajax({
                    type: "POST",
                    data: JSON.stringify({ data }),
                    url: `/bot/delete`,
                    dataType: "json",
                    contentType: 'application/json',
                    success: (res) => {
                        if (res.error) tata.error("Error", res.message);
                        else {
                            tata.success("Success", res.message);
                            $(`#${id}`).fadeOut(300);
                            $(`#${id}av`).fadeOut(300);
                            $(`#infobox`).fadeOut(300);
                        }
                        limit = false
                    }
                });
                location.replace("/");
            }
        })
    }
}
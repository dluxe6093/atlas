let express = require("express");
let app = express.Router();
let bots = require("@bots");

app.post("/", async (req, res) => {
    if(!req.user) return res.send({ error: true, message: "Sign in to continue"});

    let data = req.body.data;
    let bot = await bots.findOne({ id: data.id });

    if(!bot) return res.send({ error: true, message: "Error occured"});
    else {
        let addowners = bot.owners;
        addowners.push(req.user.id);
        if (!addowners.includes(req.user.id)) return res.send({ error: true, message: "You can't request auth token" }).statusCode(403);

        res.send({ error: false, message: `Authorization Token: ${bot.auth}`});
    }
});
module.exports = app;
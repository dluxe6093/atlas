let express = require("express");
let app = express.Router();
let { checkGuild } = require("@website")
let bots = require("@bots");

app.post("/:id/save", async (req, res) => {
    if(!req.user) return res.send({ error: true, message: "Sign in to continue"});
    

    let limit = false;
    let data = await req.body.data;
    let fetch = await req.client.users.fetch(data.id).catch(() => { });
    let bot = await bots.findOne({ id: data.id });

    let addowners = bot.owners;
    addowners.push(req.user.id);
    if (!addowners.includes(req.user.id)) return res.send({ error: true, message: 403 }).statusCode(403);

    if (!fetch) res.send({ error: true, message: "Error Occured" });
    else if (data.libraries.toString().length < 1) res.send({ error: true, message: "You can only have 1 Lib!" });
    else if (data.short.length < 50) res.send({ error: true, message: "Short Description can't be less than 50 charactars" });
    else if (data.short.length > 250) res.send({ error: true, message: "Short Description can't be more than 250 charactars" });
    else if (data.long.length < 300) res.send({ error: true, message: "Long Description can't be less than 300 charactars" });
    else if (data.long.length > 5000) res.send({ error: true, message: "Long Description can't be more than 5k charactars" });
    else if (data.prefix.length > 10) res.send({ error: true, message: "Prefix can't be more than 10 charactars" });
    else if (data.prefix.length < 1) res.send({ error: true, message: "Prefix can't be less than 1 charactars" });
    else if (data.tags.toString().split(", ").length > 5) res.send({ error: true, message: "Max tags is 5" });
    else if (data.users.length > 5) res.send({ error: true, message: "Additional Owners can't be more than 3" });
    else if (data.users.includes(req.user.id)) res.send({ error: true, message: "You can't add yourself to additional owners." });
    else {
        let owners = []
        if (data.users.length > 0) {
            for (i = 0; i < data.users.length; i++) {
                let userC = await req.client.users.fetch(data.users[i]).catch(() => { });
                if (!userC) owners.push(data.users[i])
            }
            if (owners.length > 0) return res.send({ error: true, message: `"${owners.toString()}" are invalid owners id's` });
        }

        if(limit === false) {
            limit = true;
            bot.username = fetch.username;
            bot.description = data.long;
            bot.short = data.short;
            bot.website = data.website;
            bot.donate = data.donate;
            bot.invite = data.invite;
            bot.server = data.server;
            bot.prefix = data.prefix;
            bot.nsfw = data.nsfw;
            bot.tags = data.tags;
            bot.owners = data.users
            bot.vanity = data.vanity;
            bot.announce = data.announce;
            bot.lib = data.libraries;
            bot.banner = data.banner;
            await bot.save();
            limit = false
        }
        res.send({ error: false });

        let weblog = await req.client.channels.cache.get("957429662550732850");
        weblog.send(`<@${req.user.id}> has edited **${fetch.username}**! <https://atlasbots.com/bot/${fetch.id}>`)
    }
});
module.exports = app;

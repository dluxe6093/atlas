let express = require("express");
let app = express.Router();
let { gtoken } = require("@website")
let bots = require("@bots");

app.post("/", async (req, res) => {
    if(!req.user) return res.send({ error: true, message: "Sign in to continue"});
    let limit = false;
    let data = await req.body.data
    let fetch = await req.client.users.fetch(data.id).catch(() => { });
    let bot = await bots.findOne({ id: data.id });

    if (!fetch) res.send({ error: true, message: "That bot doesn't exists. Please check the ID to make sure it's correct." });
    else if (!fetch.bot) res.send({ error: true, message: "You can't add humans" });
    else if (bot) res.send({ error: true, message: "This bot already exists" });
    else if (!req.client.guilds.cache.get("940042260010106910").members.cache.get(req.user.id)) res.send({ error: true, message: "You are not in the Atlas Bots server" });
    else if (data.libraries.toString().length < 1) res.send({ error: true, message: "You can only have 1 Lib!" });
    else if (data.short.length < 50) res.send({ error: true, message: "Short Description can't be less than 50 characters" });
    else if (data.short.length > 250) res.send({ error: true, message: "Short Description can't be more than 250 characters" });
    else if (data.long.length < 300) res.send({ error: true, message: "Long Description can't be less than 300 characters" });
    else if (data.long.length > 5000) res.send({ error: true, message: "Long Description can't be more than 5k characters" });
    else if (data.prefix.length > 10) res.send({ error: true, message: "Prefix can't be more than 10 characters" });
    else if (data.prefix.length < 1) res.send({ error: true, message: "Prefix can't be empty" });
    else if (data.tags.toString().split(", ").length > 5) res.send({ error: true, message: "Max tags is 5" });
    else if (data.users.length > 5) res.send({ error: true, message: "Additional Owners can't be more than 3" });
    else if (data.users.includes(req.user.id)) res.send({ error: true, message: "You can't add yourself to additional owners." });
    else if (!data.invite) res.send({ error: true, message: "Invite can't be empty" });
    else {
        let owners = []
        if (data.users.length > 0) {
            for (i = 0; i < data.users.length; i++) {
                let userC = await req.client.users.fetch(data.users[i]).catch(() => { });
                if (!userC) owners.push(data.users[i])
            }
            if (owners.length > 0) return res.send({ error: true, message: `"${owners.toString()}" are invalid owners id's` });
        }

        if(limit === false) {
            limit = true;
            await new bots({
                id: data.id,
                username: fetch.username,
                owner: req.user.id,
                description: data.long,
                short: data.short,
                website: data.website,
                donate: data.donate,
                invite: data.invite,
                server: data.server,
                prefix: data.prefix,
                auth: await gtoken(75),
                nsfw: data.nsfw,
                tags: data.tags,
                owners: data.users,
                lib: data.libraries
            }).save();
            limit = false
        }
        res.send({ error: false })
        let modLog = await req.client.channels.cache.get("957429662550732850");
        modLog.send(`<@${req.user.id}> has added **${fetch.username}**! <@&942659699969495081>`)
    }
});
module.exports = app;

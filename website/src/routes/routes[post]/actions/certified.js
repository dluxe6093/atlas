let express = require("express");
let app = express.Router();
let Bots = require("@bots");
let Users = require("@users")

const { MessageEmbed } = require("discord.js");

app.post("/", async function(req, res) {

    let data = req.body.data;
    // Check if user has permission
    if (!req.user) return res.send({ error: true, message: "Sign in to continue" });
    let fetch = await req.client.guilds.cache.get("940042260010106910").members.fetch({ user: req.user })
    if (!fetch) return res.send({ error: true, message: "You don't have permission to do that" });
    if (!fetch.roles.cache.has("940046847186194472")) return res.send({ error: true, message: "You don't have permission to do that" });
    // Check bot exists
    const bot = await Bots.findOne({ certified: false, id: data.id }, { _id: false });
    if (!bot) return res.send({ error: true, message: `Couldn't find the bot!` });

    //Check User exists
    const user = await Users.findOne({ id: data.owner }, { _id: false });
    if (!user) return res.send({ error: true, message: `Couldn't find the user!` });

    // Update bot in database
    await Bots.findOneAndUpdate({ id: data.id }, { $set: { certified: true } })
    await Users.findOneAndUpdate({ id: data.owner }, { $set: { certified: true } })
    let modLog = await req.client.channels.cache.get("957429662550732850");
    let botUser = await req.client.users.fetch(data.id);
    let owner = bot.owner
    modLog.send({
        content: `<@${owner}>`,
        embeds: [
            new MessageEmbed()
            .setAuthor(`Bot Certified!`)
            .addField(`Bot`, `<@${data.id}> (\`${data.id}\`)`)
            .addField(`Admin`, `${req.user.username} (\`${req.user.id}\`)`)
            .setThumbnail(botUser.displayAvatarURL({ format: "png", size: 256 }))
            .setColor("GREEN")
            .setTimestamp()
        ],
    });
    res.send({ error: false, message: `Bot certified!` });
    let m = await req.client.guilds.cache.get("940042260010106910").members.fetch({ user: owner }).catch(() => {});
    if (m) await m.roles.add("940048350969999360")

    let b = await req.client.guilds.cache.get("940042260010106910").members.fetch({ user: data.id }).catch(() => {});
    if (b) await b.roles.add("940048700477149234")
});
module.exports = app;

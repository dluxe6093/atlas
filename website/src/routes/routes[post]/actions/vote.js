let express = require("express");
let app = express.Router();
const Bots = require("@bots");
const Users = require("@users");
let { renderPage, not_found, checkAuth } = require("@website");
const { MessageEmbed } = require("discord.js");

app.post('/:id/vote', checkAuth, async (req, res) => {

    let alertNotification;
    let errorNotification;

    let modLog = await req.client.channels.cache.get("957429662550732850");

    let user = await Users.findOne({ id: req.user.id });
    let robot = await Bots.findOne({ id: req.params.id });

    if (!user) user = await new Users({ id: req.user.id });
    //if (!robot) robot = await Bots.findOne({ vanity: req.params.id });
    if (!robot) return not_found(req, res);

    let check = await user.votes.get(robot.id);
    let fetch = await req.client.users.fetch(robot.id).catch(() => {});

    if (req.body.submit) {
    
    if (!check) {

        robot.votes = robot.votes- + -1;

        user.votes.set(robot.id, Date.now());
        user.time = Date.now();
        user.botliked = robot.id;

        await robot.save();
        await user.save();

        let embed = new MessageEmbed()
         .setTitle('🎉 New Vote Submitted')
         .setColor('RANDOM')
         .setDescription(`<@${req.user.id}> [\`${req.user.id}\`] voted for  [${fetch.username}](https://atlasrobot.com/bot/${fetch.id})`)
         .addField('Vote Count', robot.votes.toString())
         .setTimestamp()
         .setFooter(`© Atlas Bot List - ${new Date().getFullYear()}`)

        modLog.send({ embeds: [embed] });

        alertNotification = `Your vote has been submitted successfully`;
    
    } else if (check) {

        if (43200000 - (Date.now() - check) < 0) {

            robot.votes = robot.votes- + -1;

            user.votes.set(robot.id, Date.now())
            user.time = Date.now();
            user.botliked = robot.id;

            await robot.save();
            await user.save();

            let embed2 = new MessageEmbed()
             .setTitle('🎉 New Vote Submitted')
             .setColor('RANDOM')
             .setDescription(`<@${req.user.id}> [\`${req.user.id}\`] voted for  [${fetch.username}](https://atlasrobot.com/bot/${fetch.id})`)
             .addField('Vote Count', robot.votes.toString())
             .setTimestamp()
             .setFooter(`© Atlas Bot List - ${new Date().getFullYear()}`)

             modLog.send({ embeds: [embed2] });

             alertNotification = `Your vote has been submitted successfully`;
        
        } else {

            let date = require('parse-ms')(43200000 - (Date.now() - check));

            errorNotification = `You must wait: ${date.hours} hours, ${date.minutes} minutes, ${date.seconds} seconds. before you can vote again!`
        }
    }
  }
  renderPage(res, req, '/actions/vote.ejs', {
      alert: alertNotification,
      error: errorNotification,
      fetch, robot
  });
});

module.exports = app;

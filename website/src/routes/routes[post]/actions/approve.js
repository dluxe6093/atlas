let express = require("express");
let app = express.Router();
let Bots = require("@bots");

const { MessageEmbed } = require("discord.js");

app.post("/", async function (req, res) {

    let data = req.body.data;
    // Check if user has permission
    if (!req.user) return res.send({ error: true, message: "Sign in to continue" });
    let fetch = await req.client.guilds.cache.get("940042260010106910").members.fetch({ user: req.user })
    if (!fetch) return res.send({ error: true, message: "You don't have permission to do that" });
    if (!fetch.roles.cache.has("940046849958629447")) return res.send({ error: true, message: "You don't have permission to do that" });
    // Check bot exists
    const bot = await Bots.findOne({ affirmed: false, id: data.id }, { _id: false });
    if (!bot) return res.send({ error: true, message: `Couldn't find the bot! It might not exist or was already approved.` });
    let userbot = await req.client.users.fetch(bot.id);
    // Update bot in database
    await Bots.findOneAndUpdate({ id: data.id }, { $set: { affirmed: true } })
    let modLog = await req.client.channels.cache.get("957429662550732850");
    let owner = bot.owner
    modLog.send({
        content: `<@${owner}>`,
        embeds: [
            new MessageEmbed()
                .setColor("57F287")
                .setAuthor(`Bot Approved`)
                .addField(`Bot`, `<@${data.id}> (\`${data.id}\`)`)
                .addField(`Moderator`, `${req.user.username} (\`${req.user.id}\`)`)
                .setThumbnail(userbot.displayAvatarURL({ format: "png", size: 256 }))
                .setColor("GREEN")
                .setTimestamp()
        ],
    });
    res.send({ error: false, message: `Bot approved!` });
    let m = await req.client.guilds.cache.get("940042260010106910").members.fetch({ user: owner }).catch(() => { });
    if (m) await m.roles.add("940048351548817428")
});
module.exports = app;

let express = require("express");
let app = express.Router();
let USERS = require("@users");

app.post("/", async (req, res) => {
    if(!req.user) return res.send({ error: true, message: "Sign in to continue"});
    
    let limit = false;
    let data = await req.body.data
    let user = await USERS.findOne({ id: req.user.id });
    if(!user) user = await new USERS({id:req.user.id}).save();

    if(data.bio.length > 150) res.send({ error: true, message: "Bio graphy can't be more than 150 charactars"})
    else if(data.github && !/^(http|https):\/\//gi.test(data.github)) res.send({ error: true, message: "Github link should have https/http"})
    else if(data.donate && !/^(http|https):\/\//gi.test(data.donate)) res.send({ error: true, message: "Donation link should have https/http"})
    else if(data.website && !/^(http|https):\/\//gi.test(data.website)) res.send({ error: true, message: "Website link should have https/http"})
    else if(data.twitter && !/^(http|https):\/\//gi.test(data.twitter)) res.send({ error: true, message: "Twitter link should have https/http"})
    else {
        if(limit == false){
            limit = true
            user.donate = data.donate
            user.github = data.github
            user.bio = data.bio
            user.website = data.website
            user.twitter = data.twitter
            await user.save();
            res.send({ error: false });
            limit = false
        }
    }
});
module.exports = app;
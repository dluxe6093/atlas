let express = require("express");
let app = express.Router();
let Bots = require("@bots");
let { renderPage, checkAuth } = require("@website");

const { MessageEmbed } = require("discord.js");

app.post("/:id/report", checkAuth, async function (req, res) {

    let alertNotification;
    let errorNotification;

    const robot = await Bots.findOne({ affirmed: true, id: req.params.id }, { _id: false });
    if (!robot) return res.send({ error: true, message: `Couldn't find the bot!` });

    let fetch = await req.client.users.fetch(robot.id).catch(() => {});

    if (!req.body.reportreason) errorNotification = "Please enter a report reason."
    else {
        // Check bot exists
        let botuser = await req.client.users.fetch(req.params.id).catch(() => { });
        if (!botuser) return errorNotification = `Couldn't find the bot!` 
                // Update bot in database
        await Bots.findOneAndUpdate({ id: req.params.id }, { $set: { reported: true, rreason: req.body.reportreason, reportedby: req.user.username } })
        let chan = await req.client.channels.cache.get("961988636050612294");
        let embed = new MessageEmbed()
        .setAuthor("Bot Report")
        .addField("Bot", `${botuser.username} (\`${botuser.id}\`)`)
        .setColor("RED")
        .addField("User", `${req.user.username} (\`${req.user.id}\`)`)
        .addField("Reason", req.body.reportreason)
        .setThumbnail(botuser.displayAvatarURL({ format: "png", size: 256 }))
        .setTimestamp()
        await chan.send({ embeds: [embed], content: `<@&942659699969495081>` })
        alertNotification = `You reported this bot successfully!`;
    }

    renderPage(res, req, '/actions/report.ejs', { alert: alertNotification, error: errorNotification, robot, fetch  });
});
module.exports = app;

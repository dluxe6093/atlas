let express = require("express");
let app = express.Router();
let Bots = require("@bots");

const { MessageEmbed } = require("discord.js");

app.post("/", async function(req, res) {

    let data = req.body.data;
    let fetch = await req.client.users.fetch(data.id).catch(() => { });

    if (!fetch) res.send({ error: true, message: "Coudn't find that bot" });
    else if (!data.rreason) res.send({ error: true, message: "Please enter a delete reason." });
    else {
        
        // Check bot exists
        const bot = await Bots.findOne({ affirmed: true, reported: true, id: data.id }, { _id: false });
        if (!bot) return res.send({ error: true, message: `Couldn't find the bot!` });;

        // Update bot in database
        await Bots.deleteOne({ id: data.id })

        let modLog = await req.client.channels.cache.get("957429662550732850");

        console.log(data.id)

        let embed = new MessageEmbed()
         .setAuthor('Bot Deleted')
         .setColor('RED')
         .addField('Bot', `<@${data.id}> (\`${data.id}\`)`)
         .addField("Moderator", `${req.user.username} (\`${req.user.id}\`)`)
         .addField('Reason', data.rreason)
         .setTimestamp(fetch.displayAvatarURL({ format: "png", size: 256 }))
         .setThumbnail()

        await modLog.send({ embeds: [embed], content: `<@${bot.owner}>` });
    }
});
module.exports = app;

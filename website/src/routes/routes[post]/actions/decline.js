let express = require("express");
let app = express.Router();
let Bots = require("@bots");

const { MessageEmbed } = require("discord.js");

app.post("/", async function (req, res) {

    let data = req.body.data;
    // Check if user has permission
    if (!req.user) return res.send({ error: true, message: "Sign in to continue" });
    let userfetch = await req.client.guilds.cache.get("940042260010106910").members.fetch({ user: req.user })
    if (!userfetch) return res.send({ error: true, message: "You don't have permission to do that" });
    if (!userfetch.roles.cache.has("940046849958629447")) return res.send({ error: true, message: "You don't have permission to do that" });
    let fetch = await req.client.users.fetch(data.id).catch(() => { });

    if (!fetch) res.send({ error: true, message: "Couldn't find that bot" });
    if (!data.reason) res.send({ error: true, message: "Please enter a decline reason." });
    else {

        // Check bot exists
        const bot = await Bots.findOne({ affirmed: false, id: data.id }, { _id: false });
        if (!bot) return res.send({ error: true, message: `Couldn't find the bot! It might not exist or was already denied.` });;

        // Update bot in database
        await Bots.deleteOne({ id: data.id })
        let userbot = await req.client.users.fetch(bot.id);
        let modLog = await req.client.channels.cache.get("957429662550732850");
        modLog.send({
            content: `<@${bot.owner}>`,
            embeds: [
                new MessageEmbed()
                    .setAuthor(`Bot Denied`)
                    .addField(`Bot`, `<@${data.id}> (\`${data.id}\`)`)
                    .addField(`Moderator`, `${req.user.username} (\`${req.user.id}\`)`)
                    .addField(`Reason`, `${data.reason}`)
                    .setThumbnail(userbot.displayAvatarURL({ format: "png", size: 256 }))
                    .setColor("RED")
                    .setTimestamp()
            ],
        });
    }
});
module.exports = app;

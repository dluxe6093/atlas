let express = require("express");
let app = express.Router();
let Bots = require("@bots");

app.post("/", async function(req, res) {

    let data = req.body.data;

    // Check bot exists
    const bot = await Bots.findOne({ affirmed: true, reported: true, id: data.id });
    if (!bot) return res.send({ error: true, message: `Couldn't find the bot!` });;

    // Update bot in database
    await Bots.findOneAndUpdate({ id: data.id }, { $set: { reported: false, rreason: " ", reportedby: " " } })
});
module.exports = app;

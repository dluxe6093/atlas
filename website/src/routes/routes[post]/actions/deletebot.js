let express = require("express");
let app = express.Router();
let bots = require("@bots");

app.post("/", async (req, res) => {
    if(!req.user) return res.send({ error: true, message: "Sign in to continue"});

    let data = req.body.data;
    let bot = await bots.findOne({ id: data.id });

    if(!bot) return res.send({ error: true, message: "Error occured"});
    else {
        let addowners = bot.owner
        if (!addowners.includes(req.user.id)) return res.send({ error: true, message: "You can't delete this bot" });

        let fetch = await req.client.users.fetch(bot.id).catch(() => {});

        await bot.deleteOne();
        res.send({ error: false, message: `${fetch.username} has been deleted`})
        let chan = await req.client.channels.cache.get("957429662550732850");
        await chan.send(`<@${req.user.id}> has deleted **${fetch.username}**.`)
        let inserver = await req.client.guilds.cache.get("940042260010106910").members.fetch(fetch.id).catch(() => {});
        if (inserver) await inserver.kick();
    }
});
module.exports = app;
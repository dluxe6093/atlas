let express = require("express");
let app = express.Router();
let { renderPage } = require("@website")
let BOTS = require("@bots");

app.get("/", async (req, res) => {
    let Topbots = await BOTS.find({ affirmed: true }).sort([['votes', 'descending']]).limit(15);
    for (i = 0; i < Topbots.length; i++) { await req.client.users.fetch(Topbots[i].id).catch(() => { }); }

    let Newbots = await BOTS.find({ affirmed: true }).sort([['date', 'descending']]).limit(9);
    for (i = 0; i < Newbots.length; i++) { await req.client.users.fetch(Newbots[i].id).catch(() => { }); }

    let Certbots = await BOTS.find({ certified: true }).sort([['descending']]).limit(9);
    for (i = 0; i < Certbots.length; i++) { await req.client.users.fetch(Certbots[i].id).catch(() => { }); }

    renderPage(res, req, 'index.ejs', { Topbots, Newbots, Certbots });
});
module.exports = app;
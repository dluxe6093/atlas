let express = require("express");
let app = express.Router();
let { renderPage } = require("@website")

app.get("/", async (req, res) => {
    renderPage(res, req, "/status/custom.ejs", {
        status: 404,
        body: "You've been lost, or you don't have the permissions to access this page!"
    }, 404)
});

module.exports = app;
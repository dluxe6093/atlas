let express = require("express");
let app = express.Router();
let config = require("@settings");

app.get("/", (req, res) => {
    res.redirect(config.server)
});

module.exports = app;
let express = require("express");
let app = express.Router();
let BOTS = require("@bots")
let { renderPage } = require("@website")

app.get("/", async (req, res) => {
    let bots;
    if (isNaN(req.query.page)) return res.redirect("/certified?page=1");
    if (req.query.page) {
        if (req.query.page == "0") res.redirect("/certified")
        else bots = await BOTS.find({ certified: true }).sort([['descending']]).skip(req.query.page > 1 ? ((req.query.page - 1) * 12) : 0).limit(12)

        for (i = 0; i < bots.length; i++) { await req.client.users.fetch(bots[i].id).catch(() => { }); }
        renderPage(res, req, '/pages/cert.ejs', { bots });
    } else return res.redirect("/certified?page=1");
});
module.exports = app;
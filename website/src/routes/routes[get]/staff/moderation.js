let express = require("express");
let app = express.Router();
let { renderPage, no_access, checkAuth } = require("@website");

app.get("/", checkAuth, async (req, res) => {

    let moderator = await req.client.guilds.cache.get("940042260010106910").members.fetch(req.user.id);
    if (!moderator.roles.cache.has("940046849958629447")) return no_access(req, res);

    renderPage(res, req, '/staff/moderation.ejs');
});
module.exports = app;

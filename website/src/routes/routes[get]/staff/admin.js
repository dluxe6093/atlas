let express = require("express");
let app = express.Router();
let { renderPage, no_access, checkAuth } = require("@website");
let BOTS = require("@bots");

app.get("/:id/admin", checkAuth, async (req, res) => {

    let moderator = await req.client.guilds.cache.get("940042260010106910").members.fetch(req.user.id);
    if (!moderator.roles.cache.has("940046847186194472")) return no_access(req, res);

    let robot = await BOTS.findOne({ id: req.params.id })
    if (!robot) return not_found(req, res);

    renderPage(res, req, '/staff/admin.ejs', {robot});
});
module.exports = app;

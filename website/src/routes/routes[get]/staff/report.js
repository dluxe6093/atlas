let express = require("express");
let app = express.Router();
let { renderPage, no_access, checkAuth } = require("@website");
let BOTS = require("@bots");

app.get("/", checkAuth, async (req, res) => {

    let moderator = await req.client.guilds.cache.get("940042260010106910").members.fetch(req.user.id);
    if (!moderator.roles.cache.has("940046849958629447")) return no_access(req, res);

    let bots = await BOTS.find({ reported: "true" }, { _id: false });
    for (i = 0; i < bots.length; i++) { await req.client.users.fetch(bots[i].id).catch(() => { }); }

    renderPage(res, req, '/staff/report.ejs', {bots});
});
module.exports = app;

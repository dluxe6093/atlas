let express = require("express");
let app = express.Router();
let BOTS = require("@bots")
let { renderPage } = require("@website")

app.get("/", async (req, res) => {
    let bots;
    if (isNaN(req.query.page)) return res.redirect("/bots?page=1");
    if (req.query.page) {
        if (req.query.page == "0") res.redirect("/bots")
        else bots = await BOTS.find({ affirmed: true }).sort([['votes', 'descending']]).skip(req.query.page > 1 ? ((req.query.page - 1) * 12) : 0).limit(12)

        for (i = 0; i < bots.length; i++) { await req.client.users.fetch(bots[i].id).catch(() => { }); }
        renderPage(res, req, '/static/bots.ejs', { bots });
    } else return res.redirect("/bots?page=1");
});
module.exports = app;
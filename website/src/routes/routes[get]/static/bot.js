let express = require("express");
let app = express.Router();
let { renderPage, not_found, checkAuth } = require("@website");
let BOTS = require("@bots");
const hljs = require("highlight.js")
const { Remarkable } = require('remarkable');

app.get("/:id", async (req, res) => {
    let robot = await BOTS.findOne({ id: req.params.id });

    if (!robot) {
        robot = await BOTS.findOne({ vanity: req.params.id, certified: true });
    }

    if(!robot) return not_found(req, res);

    let owners = [robot.owner].concat(robot.owners);

    try {
        owners = (
          await req.client
            .guilds.cache.get("940042260010106910")
            .members.fetch({ user: owners })
        ).map((x) => {
          return x.user;
        });
      } catch (e) {}

    const md = new Remarkable({
        html: true,
        xhtmlOut: true,
        breaks: true,
        highlight: function (str, lang) {
            if (lang && hljs.getLanguage(lang)) {
                try {
                    return hljs.highlight(lang, str).value;
                } catch (err) {}
            }
            try {
                return hljs.highlight(str).value;
            } catch (err) {}
            return '';
        }
    })

    let fetch = await req.client.users.fetch(robot.id).catch(() => {});
    renderPage(res, req, '/static/bot.ejs', { robot, fetch, md, owners });
});

module.exports = app;

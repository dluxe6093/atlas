let express = require("express");
let app = express.Router();
let { renderPage, no_access, checkAuth } = require("@website");
let BOTS = require("@bots");

app.get("/", checkAuth, async (req, res) => {

    let moderator = await req.client.guilds.cache.get("940042260010106910").members.fetch(req.user.id);
    if (!moderator.roles.cache.has("940046849547591721")) return no_access(req, res);

    let Newbots = await BOTS.find({ id: "431893326892105758" }).sort([['descending']]).limit(10);
    for (i = 0; i < Newbots.length; i++) { await req.client.users.fetch(Newbots[i].id).catch(() => { }); }

    renderPage(res, req, '/static/test.ejs', {Newbots});
});
module.exports = app;

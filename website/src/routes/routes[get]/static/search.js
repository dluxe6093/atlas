let express = require("express");
let app = express.Router();
let BOTS = require("@bots")
let { renderPage } = require("@website")

app.get("/", async (req, res) => {
    let bots;
    if(isNaN(req.query.page)) return res.redirect(`/search?q=${req.query.q}&page=1`);
    if(req.query.q){
      if(req.query.page == "0") res.redirect(`/search?q=${req.query.q}&page=1`);
      else bots = await BOTS.find({affirmed: true}).skip(req.query.page > 1 ? ((req.query.page - 1) * 12) : 0)
      for(i = 0; i < bots.length; i++){ 
        await req.client.users.fetch(bots[i].id).catch(() => {});
      }

      if(req.query.q !== "all"){
        bots = bots.filter(bot => {
          if (req.client.users.cache.get(bot.id) && req.client.users.cache.get(bot.id).username.toLowerCase().includes(req.query.q.toLowerCase())) return true;
          else if (bot.tags && bot.tags.toString().toLowerCase().includes(req.query.q ? req.query.q.toLowerCase() : "")) return true;
          else if (bot.short && bot.short.toLowerCase().includes(req.query.q ? req.query.q.toLowerCase() : "")) return true;
          else if (bot.lib && bot.lib.toString().toLowerCase().includes(req.query.q ? req.query.q.toLowerCase() : "")) return true;
          else return false;
        })
      }
      renderPage(res, req, '/static/search.ejs', { bots });
    } else res.redirect(`/search?q=all&page=1`);
});
module.exports = app;
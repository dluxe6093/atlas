let express = require("express");
let app = express.Router();
let { renderPage } = require("@website");

app.get("/", async (req, res) => {
    renderPage(res, req, '/static/tos.ejs');
});
module.exports = app;

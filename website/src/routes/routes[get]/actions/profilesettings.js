let express = require("express");
let app = express.Router();
let { renderPage, checkAuth } = require("@website")

let USERS = require("@users");

app.get("/:id/edit", checkAuth, async (req, res) => {
    let duser = await USERS.findOne({ id: req.user.id});
    if(!duser) duser = await new USERS({id: req.user.id}).save();

    let uuser = await USERS.findOne({ id: req.params.id});
    if (!uuser) return not_found(req, res);
    renderPage(res, req, '/actions/profilesettings.ejs', { duser });
});
module.exports = app;
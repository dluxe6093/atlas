let express = require("express");
let app = express.Router();
let { renderPage, checkAuth, not_found } = require("@website")
let BOTS = require("@bots");

app.get("/:id/edit", checkAuth, async (req, res) => {
    let robot = await BOTS.findOne({ id: req.params.id })

    if (!robot) {
      robot = await BOTS.findOne({ vanity: req.params.id, certified: true });
  }
  
    if (!robot) return not_found(req, res);

    let owner = robot.owners;

    try {
        owner = (
          await req.client
            .guilds.cache.get("869645628018405446")
            .members.fetch({ user: owner })
        ).map((x) => {
          return x.user;
        });
      } catch (e) {}

    let owners = robot.owners
    owners.push(req.user.id)
    if (!owners.includes(req.user.id)) return not_found(req, res);
    else owners.pull(req.user.id)
    await req.client.users.fetch(robot.id).catch(() => { });

    renderPage(res, req, '/actions/editbot.ejs', { robot, owner });
});
module.exports = app;
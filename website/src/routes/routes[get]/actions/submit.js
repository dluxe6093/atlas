let express = require("express");
let app = express.Router();
let { renderPage, checkAuth } = require("@website")

app.get("/", checkAuth, async (req, res) => {
    renderPage(res, req, '/actions/submit.ejs');
});
module.exports = app;
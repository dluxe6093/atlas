let express = require("express");
let app = express.Router();
let { renderPage, checkAuth } = require("@website")
let BOTS = require("@bots");
let USERS = require("@users");

app.get("/:id", async (req, res) => {
    let bots = await BOTS.find({ owner: req.params.id });
    let abots = await BOTS.find({ owners: [req.params.id] })
    for (i = 0; i < abots.length; i++) { await req.client.users.fetch(abots[i].id).catch(() => { }); }
    for (i = 0; i < bots.length; i++) { await req.client.users.fetch(bots[i].id).catch(() => { }); }

    let duser = await USERS.findOne({ id: req.params.id});
    if(!duser) duser = await new USERS({id: req.user.id}).save();

    let userProfile = await req.client.users.fetch(req.params.id);
    if (!userProfile) return not_found(req, res);
    renderPage(res, req, '/actions/profile.ejs', { bots, abots, duser, userProfile });
});
module.exports = app;

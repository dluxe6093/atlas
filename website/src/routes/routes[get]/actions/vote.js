let express = require("express");
let app = express.Router();
let { renderPage, not_found, checkAuth } = require("@website");
let BOTS = require("@bots");

app.get("/:id/vote", checkAuth, async (req, res) => {

    let alertNotification;
    let errorNotification;

    let robot = await BOTS.findOne({ id: req.params.id });

    if (!robot) {
        robot = await BOTS.findOne({ vanity: req.params.id, certified: true });
    }
    
    if(!robot) return not_found(req, res);

    let fetch = await req.client.users.fetch(robot.id).catch(() => {});
    renderPage(res, req, '/actions/vote.ejs', { robot, fetch, alert: alertNotification, error: errorNotification, });
});
module.exports = app;

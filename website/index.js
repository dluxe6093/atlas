const express = require("express");
const app = express();
const ejs = require("ejs");
const path = require("path");
const passport = require("passport");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const MongoStore = require('connect-mongo')(session);
const Strategy = require("passport-discord").Strategy;
const config = require("@settings");
const { MessageEmbed } = require("discord.js");
const dataDir = path.resolve(`${process.cwd()}${path.sep}website`);
const templateDir = path.resolve(`${dataDir}${path.sep}src/dynamic`);
const url = require("url");


module.exports = async (client) => {
    app.use('/static', express.static(path.join(__dirname, 'public'))) // public directory
    app.use('/sitemap.xml', express.static(path.join(__dirname, 'public/assets/others/sitemap.xml'))) // sitemap
    app.use('/robots.txt', express.static(path.join(__dirname, 'public/assets/others/robots.txt'))) // robots.txt

    passport.serializeUser((user, done) => done(null, user));
    passport.deserializeUser((obj, done) => done(null, obj));

    passport.use(new Strategy({
        clientID: config.id,
        clientSecret: config.clientSecret,
        callbackURL: `${config.domain}/callback`,
        scope: ["identify"],
    },
        (accessToken, refreshToken, profile, done) => {
            process.nextTick(() => done(null, profile));
        }));

    app.use(session({
        cookie: { maxAge: require("ms")("10 years") },
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
        secret: "#@%#&^$^$%@$^$&%#$%@#$%$^%&$%^#$%@#$%#E%#%@$FEErfgr3g#%GT%536c53cc6%5%tv%4y4hrgrggrgrgf4n",
        resave: false,
        saveUninitialized: false,
    }));


    app.use(passport.initialize());
    app.use(passport.session());
    app.use(cookieParser());

    app.locals.domain = config.domain.split("//")[1];


    app.engine("html", ejs.renderFile);
    app.set("view engine", "html");
    app.set("views", __dirname + "/dynamic");


    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));


    app.get("/login", (req, res, next) => {
        if (req.session.backURL) req.session.backURL = req.session.backURL;
        else if (req.headers.referer) {
            const parsed = url.parse(req.headers.referer);
            if (parsed.hostname === app.locals.domain) req.session.backURL = parsed.path;
        } else req.session.backURL = '/';
        next();
    },
        passport.authenticate("discord", { prompt: "none" }));

    app.get("/callback", passport.authenticate("discord", { failureRedirect: "/" }), (req, res) => {
        res.redirect(req.session.url || "/");

        // const user = client.users.fetch(req.user.id)

        // client.guilds.cache.get("940058220712591430").channels.cache.get("942638304006926377").send({
        //     content: `Authenticated user!`,
        //     embeds: [
        //         new MessageEmbed()
        //         .setThumbnail(user.displayAvatarURL({ format: "png", size: 256, dynamic: true }))
        //         .setColor("RANDOM")
        //         .addField("User", req.user.username)
        //         .addField("User ID", req.user.id)
        //         .setTimestamp()
        //     ]
        // })
    });

    app.use(async (req, res, next) => {
        req.user = req.isAuthenticated() ? await client.users.fetch(req.user.id) : null;
        req.client = client;
        req.redirect = null;
        next();
    });


    app.get("/logout", (req, res) => {
        req.session.destroy(() => {
            req.logout();
            res.redirect("/");
        });
    });

    const apps_get = [
        {
            url: "/",
            source: require("./src/routes/routes[get]/index.js")
        },
        {
            url: "/legal",
            source: require("./src/routes/routes[get]/static/tos.js")
        },
        {
            url: "/discord",
            source: require("./src/routes/routes[get]/redirects/discord.js")
        },
        {
            url: "/user",
            source: require("./src/routes/routes[get]/actions/profile.js")
        },
        {
            url: "/bot/add",
            source: require("./src/routes/routes[get]/actions/submit.js")
        },
        {
            url: "/user",
            source: require("./src/routes/routes[get]/actions/profilesettings.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[get]/actions/editbot.js")
        },
        {
            url: "/bots",
            source: require("./src/routes/routes[get]/static/bots.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[get]/static/bot.js")
        },
        {
            url: "/search",
            source: require("./src/routes/routes[get]/static/search.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[get]/actions/vote.js")
        },
        {
            url: "/panel",
            source: require("./src/routes/routes[get]/staff/moderation.js")
        },
        {
            url: "/panel/queue",
            source: require("./src/routes/routes[get]/staff/queue.js")
        },
        {
            url: "/panel/report",
            source: require("./src/routes/routes[get]/staff/report.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[get]/actions/report.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[get]/staff/admin.js")
        },
        {
            url: "/certified",
            source: require("./src/routes/routes[get]/pages/cert.js")
        },
        {
            url: "/test",
            source: require("./src/routes/routes[get]/static/test.js")
        }
    ];
    const apps_post = [
        {
            url: "/bot/add",
            source: require("./src/routes/routes[post]/actions/submit.js")
        },
        {
            url: "/profile/settings",
            source: require("./src/routes/routes[post]/actions/profilesettings.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[post]/actions/editbot.js")
        },
        {
            url: "/bot/auth",
            source: require("./src/routes/routes[post]/actions/botpage.js")
        },
        {
            url: "/bot/delete",
            source: require("./src/routes/routes[post]/actions/deletebot.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[post]/actions/vote.js")
        },
        {
            url: "/bot/approve",
            source: require("./src/routes/routes[post]/actions/approve.js")
        },
        {
            url: "/bot/decline",
            source: require("./src/routes/routes[post]/actions/decline.js")
        },
        {
            url: "/bot/report/delete",
            source: require("./src/routes/routes[post]/actions/reportDelete.js")
        },
        {
            url: "/bot",
            source: require("./src/routes/routes[post]/actions/report.js")
        },
        {
            url: "/bot/report/remove",
            source: require("./src/routes/routes[post]/actions/removeReport.js")
        },
        {
            url: "/bot/certify",
            source: require("./src/routes/routes[post]/actions/certified.js")
        },
        {
            url: "/bot/certify/remove",
            source: require("./src/routes/routes[post]/actions/rcert.js")
        }
    ]
    apps_get.map(route => app.use(route.url, route.source));
    apps_post.map(route => app.use(route.url, route.source));
    //status handle
    app.use("*", require("./src/routes/routes[get]/redirects/404.js"));
    app.use((error, req, res, next) => {
        console.log(error.toString());
        // client.guilds.cache.get(config.guildID).channels.cache.get(config.bugs).send({
        //     embeds: [
        //         new MessageEmbed()
        //             .setColor("RED")
        //             .setTitle("Someone found an error at the site while browsing")
        //             .setDescription(`\`\`\`js\n${error.toString()}\`\`\``)
        //     ]
        // })
        return res.status(500).render(`${templateDir}${path.sep}/status/error.ejs`);
    });
    //sitemap generator
    (new (require("../sitemap"))()).start()
    //start app
    app.listen(config.port, null, null, () => console.log(`Listening to port "${config.port}" 🚀`));
};
const renderPage = async (res, req, template, data = {}, status = 200) => {
    const baseData = {
        bot: req.client,
        path: req.path,
        user: req.user,
        headerPath: `${dataDir}${path.sep}src/partials/header`,
        footerPath: `${dataDir}${path.sep}src/partials/footer`,
        scriptsPath: `${dataDir}${path.sep}src/partials/scripts`,
        alertsPath: `${dataDir}${path.sep}src/partials/alerts`,
        navPath: `${dataDir}${path.sep}src/partials/navbar`,
        dynamic: `${dataDir}${path.sep}src/dynamic`
    };
    let list = await req.client.guilds.cache.get("940058220712591430").bans.fetch();
    if (res.statusCode === 500) {
        res.status(500).render(path.resolve(`${templateDir}${path.sep}/status/custom.ejs`), Object.assign(baseData, {
            status: 500,
            body: "Unexpected error occurred."
        }, data));
    } else if (config.building) {
        res.render(path.resolve(`${templateDir}${path.sep}/status/maintaince.ejs`), Object.assign(baseData, data));
    } else if (req.isAuthenticated() && list.has(req.user.id) == true || req.db_user && req.db_user.banned) {
        res.render(path.resolve(`${templateDir}${path.sep}/status/banned.ejs`), Object.assign(baseData, data));
    } else  if (config.beta_over) {
        res.render(path.resolve(`${templateDir}${path.sep}/status/beta_over.ejs`), Object.assign(baseData, data)); 
    } else res.status(status).render(path.resolve(`${templateDir}${path.sep}${template}`), Object.assign(baseData, data));
};
// exports
module.exports.renderPage = renderPage;

module.exports.checkAuth = (req, res, next) => {
    if (req.isAuthenticated()) return next();
    req.session.backURL = req.url;
    res.redirect("/login");
}

module.exports.not_found = (req, res) => {
    renderPage(res, req, "/status/custom.ejs", {
        status: 404,
        body: "This page doesn't exist. Please check your URL and try again."
    }, 404)
}

module.exports.no_access = (req, res) => {
    renderPage(res, req, "/status/custom.ejs", {
        status: 404,
        body: "You don't have the permissions to access this page!"
    }, 404)
}

module.exports.gtoken = async (length) => {
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];  
    for (var i=0; i<length; i++) {
        var j = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
  }

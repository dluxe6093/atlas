const { connect } = require("mongoose");
const { readdirSync } = require("fs");
const { join } = require("path");
const filePath2 = join(__dirname, "..", "events");
const eventFiles2 = readdirSync(filePath2);
const startDashboard = require("../website/index");
const USERS = require("@users")


module.exports = (client, member) => {
   startDashboard(client);
   client.user.setActivity(`Unapproved Bots`, { type: 'LISTENING' })
   console.log(`Signed in as ${client.user.username} - ${eventFiles2.length} event(s) & ${client.commands.size} command(s) has been Loaded`);
   connect(client.config.mongodb_url, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
   }, (err) => {
      if (err) return console.error(err);
      console.log("Database has been connected")
   });

   const guild = client.guilds.cache.get("940042260010106910");
   const admin = "940046847186194472"
   const developer = "940046849547591721"
   const moderator = "940046849958629447"

   const user = guild.members?.cache.get(member.id);

   if (user?.roles.cache.has(admin)) {
      USERS.findOneAndUpdate({ id: member.id }, { $set: { admin: true } })
      console.log("admin")
   } else {
      if (!user?.roles.cache.has(admin)) {
         USERS.findOneAndUpdate({ id: member.id }, { $set: { admin: false } })
      }
   }

   if (user?.roles.cache.has(developer)) {
      USERS.findOneAndUpdate({ id: member.id }, { $set: { developer: true } })
      console.log("developer")
   } else {
      if (!user?.roles.cache.has(developer)) {
         USERS.findOneAndUpdate({ id: member.id }, { $set: { developer: false } })
      }
   }

   if (user?.roles.cache.has(moderator)) {
      USERS.findOneAndUpdate({ id: member.id }, { $set: { moderator: true } })
      console.log("moderator")
   } else {
      if (!user?.roles.cache.has(moderator)) {
         USERS.findOneAndUpdate({ id: member.id }, { $set: { moderator: false } })
      }
   }
}

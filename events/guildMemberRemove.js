const BOT = require("@bots")

module.exports = async (client, member) => {
    let bots = await BOT.find({ "owner": member.user.id })

    for (let bot of bots) {
        await BOT.deleteOne({ id: bot.botid})
        try {
            let bot_member = await client.guilds.cache.get("869645628018405446").members.fetch(bot.botid)

            bot_member.Kick();
        } catch (e) {}
    }
}
const missingPerms = (member, perms) => {
    const missingPerms = member.permissions.missing(perms)
        .map(str => `\`${str.replace(/_/g, ' ').toLowerCase().replace(/\b(\w)/g, char => char.toUpperCase())}\``);

    return missingPerms.length > 1 ?
        `${missingPerms.slice(0, -1).join(", ")}, ${missingPerms.slice(-1)[0]}` :
        missingPerms[0];
}

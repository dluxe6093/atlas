//web settings
/*
     IMPORTANT INFORMATION:
        The settings in this file may not take effect throughout the entire site and may need to be manually changed.
        Please also rename this file to settings.js for it to work (since it's git ignored to prevent sensitive info from reaching the repo).
*/
module.exports = {
    //Configuration
    port: 5869,
    token: "",
    domain: "http://localhost:5869",
    clientSecret: "something",
    id: "clientid",
    mongodb_url: "mongodb://something",
    building: false,
    beta_over: false,

    //Bot
    communityManager: ["array", "of", "discord", "ids"],
    owners: ["same", "thing", "here"],
    prefix: ">",
    server: "https://discord.gg/something",
    description: "Atlas Cord is a website where you can find bots to invite to your discord server or add your own bot to be popular in servers around Discord. You can invite bots to private servers, public servers, or simply advertise your own bot.",

    // Roles & Channels
    sguildID: "testing guild id",
    guildID: "main guild id",
    developer: "developer role id (in main)",
    // Some testing channels, they don't do anything.
    // Log channel IDs can be found in their respective files in this repo.
    testing1: "940051667020292127",
    testing2: "940051748356259840",
    testing3: "883530106654564378",
    bugs: "942634781085155358",

    //Tags
    tags: ['Social', 'Fun', 'Moderation', 'Dashboard', 'Utility', 'NSFW', 'Trivia', 'Multi','Economy', 'Giveaway', 'Security', 'Leveling', 'Games', 'Music', 'Role Play', 'Role Management', 'Reddit', 'Logging', 'Quiz', 'Configuration', 'AntiRaid', 'Crypto', 'Soundboard', 'Streaming', 'Media', 'NQN', 'Poll' ],
    libraries: ['Discord.JS', 'Discord.Net', 'discord.py', 'Orca', 'DSharpPlus', 'Discord++', 'discljord', 'discordcr', 'nyxx', 'coxir', 'DiscordGo', 'Javacord', 'Discord.jl', 'Discordia', 'Other'],
    
    //HCaptcha 
    site_key: "something",
    secret: "something"
}
